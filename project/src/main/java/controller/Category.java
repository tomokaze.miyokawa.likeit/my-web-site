package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TeamDataBeans;
import beans.UserDataBeans;
import dao.TeamDao;
import dao.UserDao;


@WebServlet("/Category")
public class Category extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public Category() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");

		
		if(user == null) {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		
		
		TeamDao teamDao = new TeamDao();
		List<TeamDataBeans> teamList = teamDao.findAll();

		request.setAttribute("teamList", teamList);
		
		
		UserDao userDao = new UserDao();
		List<UserDataBeans> userList = userDao.findAll();
		
		request.setAttribute("userList", userList);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/category.jsp");
		dispatcher.forward(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
	}

}
