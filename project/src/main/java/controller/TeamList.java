package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TeamDataBeans;
import beans.UserDataBeans;
import dao.TeamDao;
import dao.UserTeamDao;

/**
 * Servlet implementation class TeamList
 */
@WebServlet("/TeamList")
public class TeamList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TeamList() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");

		
		if(user == null) {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		
		TeamDao teamDao = new TeamDao();
		List<TeamDataBeans> teamList = teamDao.findAll();

		
		request.setAttribute("teamList", teamList);

		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/teamlist.jsp");
		dispatcher.forward(request, response);
		
		
		
	}
	
	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");
		int userId = user.getId();
		

		String[] checkedTeams = request.getParameterValues("checkbox");

		
		UserTeamDao userTeamDao = new UserTeamDao();
		userTeamDao.updateTeam(userId, checkedTeams);

		response.sendRedirect("UserData");

		
	}
}
