package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;


@WebServlet("/Registration")
public class Registration extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public Registration() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registration.jsp");
		dispatcher.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		request.setCharacterEncoding("UTF-8");

		String loginid = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");

		UserDao userDao = new UserDao();
		String loginId2 = userDao.findByLoginId(loginid);

		
		if(loginId2!=null) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(!password.equals(password2)) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(loginid.equals("") || password.equals("") || password2.equals("") || name.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registration.jsp");
			dispatcher.forward(request, response);
			return;
		}
		

		String pass =userDao.code(password);

		userDao.registration(loginid,pass,name);
		
		UserDataBeans user = (UserDataBeans) userDao.findByLoginInfo(loginid, pass);

		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);


		response.sendRedirect("Category");
		
		
		
	}

}
