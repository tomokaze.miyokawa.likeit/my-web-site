package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PostDataBeans;
import beans.TeamDataBeans;
import beans.UserDataBeans;
import dao.PostDao;
import dao.TeamDao;


@WebServlet("/Post")
public class Post extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public Post() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");

		
		if(user == null) {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
        String id = request.getParameter("id");
		
		
		TeamDao teamDao = new TeamDao();
		TeamDataBeans team = teamDao.findByteamId(id);

		request.setAttribute("team", team);
		
		
		PostDao postDao = new PostDao();
		List<PostDataBeans> postList = postDao.findAll(id);

		request.setAttribute("postList", postList);
		
	
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/post.jsp");
		dispatcher.forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		
		
		String id = request.getParameter("id");
		String title = request.getParameter("title");
		String titleBody = request.getParameter("title_body");
		
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans) session.getAttribute("userInfo");
		int userId = user.getId();
		
		
		
		if(!title.equals("") && !titleBody.equals("")) {
			
			PostDao postDao1 = new PostDao();
			postDao1.add(userId,id,title,titleBody);
			
		}else {
			request.setAttribute("errMsg", "タイトルまたは本文が入力されていません。");
			
		}
		
		
	
		
		TeamDao teamDao = new TeamDao();
		TeamDataBeans team = teamDao.findByteamId(id);

		request.setAttribute("team", team);
		
		
		PostDao postDao = new PostDao();
		List<PostDataBeans> postList = postDao.findAll(id);

		request.setAttribute("postList", postList);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/post.jsp");
		dispatcher.forward(request, response);
		
	}

}
