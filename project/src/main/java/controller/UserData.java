package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import beans.UserTeamDataBeans;
import dao.UserDao;
import dao.UserTeamDao;


@WebServlet("/UserData")
public class UserData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public UserData() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");

		
		if(user == null) {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		
		String id = request.getParameter("id");
		
		if(id==null) {
			HttpSession session1 = request.getSession();
			UserDataBeans user1=(UserDataBeans) session1.getAttribute("userInfo");
			int id_int =user1.getId();
			id=Integer.toString(id_int);
		}

		UserDao userDao = new UserDao();
		UserDataBeans user1 = userDao.information(id);
		
		request.setAttribute("user", user1);
		
		
		
		UserTeamDao userteamDao = new UserTeamDao();
		List<UserTeamDataBeans> userteamList;
		try {
			
			userteamList = userteamDao.findAll(id);
			
			request.setAttribute("userteamList", userteamList);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdata.jsp");
		dispatcher.forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginid = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");


		UserDao userDao = new UserDao();
		
		
		if(!password.equals(password2)) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			
				HttpSession session = request.getSession();
				UserDataBeans user=(UserDataBeans) session.getAttribute("userInfo");
				int id_int =user.getId();
				String id=Integer.toString(id_int);

			UserDataBeans user1 = userDao.information(id);
			
			request.setAttribute("user", user1);
			
			UserTeamDao userteamDao = new UserTeamDao();
			List<UserTeamDataBeans> userteamList;
			try {
				
				userteamList = userteamDao.findAll(id);
				
				request.setAttribute("userteamList", userteamList);
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdata.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(password.equals("") || password2.equals("") || name.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			HttpSession session = request.getSession();
			UserDataBeans user=(UserDataBeans) session.getAttribute("userInfo");
			int id_int =user.getId();
			String id=Integer.toString(id_int);

		UserDataBeans user1 = userDao.information(id);
		
		request.setAttribute("user", user1);
		
		UserTeamDao userteamDao = new UserTeamDao();
		List<UserTeamDataBeans> userteamList;
		try {
			
			userteamList = userteamDao.findAll(id);
			
			request.setAttribute("userteamList", userteamList);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdata.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		
		String pass =userDao.code(password);

		userDao.update(loginid,pass,name);

		
		UserDataBeans user1 = userDao.findByLoginInfo(loginid, pass);
		

		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user1);
		
		response.sendRedirect("UserData");
	}

}
