package beans;

import java.io.Serializable;


public class UserDataBeans implements Serializable {
		private int id;
		private String loginId;
		private String name;
		private String password;
		
		
		public UserDataBeans(String loginId, String name, int id) {
			this.id = id;
			this.loginId = loginId;
			this.name = name;
		}
		
		
		public UserDataBeans(int id, String loginId, String name, String password) {
			this.id = id;
			this.loginId = loginId;
			this.name = name;
			this.password = password;
		}


		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getLoginId() {
			return loginId;
		}

		public void setLoginId(String loginId) {
			this.loginId = loginId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

}
