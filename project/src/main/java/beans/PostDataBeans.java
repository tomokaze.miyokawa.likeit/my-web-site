package beans;

import java.io.Serializable;
import java.sql.Date;

public class PostDataBeans implements Serializable{
	
	private int id;
	private String userId;
	private String teamId;
	private String title;
	private String title_body;
	private Date create_date;
	
	private String name;
	
	

	public PostDataBeans(int id, String userId2, String teamId2, String name, String title, String title_body, Date createdate) {
		super();
		this.id = id;
		this.userId = userId2;
		this.teamId = teamId2;
		this.name = name;
		this.title = title;
		this.title_body = title_body;
		this.create_date = createdate;
		
	}
	
	
	public PostDataBeans(int id2, String userId2, String teamId2, String title2, String titleBody, Date createDate) {
		
		super();
		this.id = id2;
		this.userId = userId2;
		this.teamId = teamId2;
		this.title = title2;
		this.title_body = titleBody;
		this.create_date = createDate;
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTeamId() {
		return teamId;
	}
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle_body() {
		return title_body;
	}
	public void setTitle_body(String title_body) {
		this.title_body = title_body;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date createdate) {
		this.create_date = createdate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}