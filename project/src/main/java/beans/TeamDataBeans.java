package beans;

import java.io.Serializable;

public class TeamDataBeans implements Serializable{
	
	private int id;
	private String leagueid;
	private String logo;
	private String name;
	private String league;
	private String area;
	private String teamURL;
	private String fanclubURL;






	public TeamDataBeans(int id, String leagueid, String logo, String name) {
		super();
		this.id = id;
		this.leagueid = leagueid;
		this.logo = logo;
		this.name = name;
	}
	
	
	
	public TeamDataBeans(int id, String leagueid, String logo, String name, String league, String area) {
		super();
		this.id = id;
		this.leagueid = leagueid;
		this.logo = logo;
		this.name = name;
		this.league = league;
		this.area = area;
	}
	



	public TeamDataBeans() {
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLeagueid() {
		return leagueid;
	}
	public void setLeagueid(String leagueid) {
		this.leagueid = leagueid;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLeague() {
		return league;
	}

	public void setLeague(String league) {
		this.league = league;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getTeamURL() {
		return teamURL;
	}

	public void setTeamURL(String teamURL) {
		this.teamURL = teamURL;
	}

	public String getFanclubURL() {
		return fanclubURL;
	}
	
	public void setFanclubURL(String fanclubURL) {
		this.fanclubURL = fanclubURL;
	}

	
}
