package beans;

import java.io.Serializable;

public class UserTeamDataBeans implements Serializable{
	
	private int id;
	private int teamId;
	private int userId;
	private int leagueid;
	private String logo;
	private String name;
	private String league;
	private String area;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTeamId() {
		return teamId;
	}
	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getLeagueid() {
		return leagueid;
	}
	public void setLeagueid(int leagueid) {
		this.leagueid = leagueid;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLeague() {
		return league;
	}
	public void setLeague(String league) {
		this.league = league;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
}
