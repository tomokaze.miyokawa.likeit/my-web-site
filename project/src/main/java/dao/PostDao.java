package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.PostDataBeans;

public class PostDao {
	
	public List<PostDataBeans> findAll(String Id) {
        Connection conn = null;
        List<PostDataBeans> postList = new ArrayList<PostDataBeans>();

        try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM post"
            		+ " JOIN  user"
            		+ " ON post.user_id = user.id"
            		+ " WHERE team_id = ?"
            		+ " ORDER BY create_date DESC";
            

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Id);
			ResultSet rs = pStmt.executeQuery();


            
            while (rs.next()) {
                int id = rs.getInt("id");
                String userId = rs.getString("user_id");
                String teamId = rs.getString("team_id");
                String name = rs.getString("name");
                String title = rs.getString("title");
                String titleBody = rs.getString("title_body");
                Date createDate = rs.getDate("create_date");
                PostDataBeans post = new PostDataBeans(id, userId, teamId, name, title, titleBody, createDate);

                postList.add(post);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return postList;
    }
	
	
	public void add(int userId, String teamId, String title, String titleBody) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO post (user_id, team_id, title, title_body, create_date) VALUE(?,?,?,?,now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			pStmt.setString(2, teamId);
			pStmt.setString(3, title);
			pStmt.setString(4, titleBody);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}
	
	
	public PostDataBeans findbypostId(String Id) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM post WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int Id1 = rs.getInt("id");
			String userId = rs.getString("user_id");
			String teamId = rs.getString("team_id");
			String title = rs.getString("title");
			String titleBody = rs.getString("title_body");
			Date createDate = rs.getDate("create_date");

			return new PostDataBeans(Id1, userId, teamId, title, titleBody, createDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	
	
	public void delete(String Id) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "DELETE FROM post WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

}
