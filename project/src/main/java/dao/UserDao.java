package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import beans.UserDataBeans;

public class UserDao {
	
	public UserDataBeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			
			conn = DBManager.getConnection();


			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";


			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();


			if (!rs.next()) {
				return null;
			}
			
			int userId = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			System.out.println("id:"+userId+" loginid:"+loginIdData+" name:"+nameData);
			
			return new UserDataBeans(loginIdData, nameData, userId);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	
	
	
	 public List<UserDataBeans> findAll() {
	        Connection conn = null;
	        List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

	        try {
	            conn = DBManager.getConnection();

	            String sql = "SELECT * FROM user";


	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	           
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String loginId = rs.getString("login_id");
	                String name = rs.getString("name");
	                String password = rs.getString("password");
	                UserDataBeans user = new UserDataBeans(id, loginId, name, password);

	                userList.add(user);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return userList;
	    }
	 
	 
		public void registration(String loginId, String password, String name) {
			Connection conn = null;

			try {

				conn = DBManager.getConnection();

				String sql = "INSERT INTO user (login_id, password, name) VALUE(?,?,?)";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				pStmt.setString(2, password);
				pStmt.setString(3, name);

				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();

			} finally {

				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}
		}
		
		
		public String findByLoginId(String loginId) {
			Connection conn = null;
			try {

				conn = DBManager.getConnection();

				String sql = "SELECT * FROM user WHERE login_id = ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				ResultSet rs = pStmt.executeQuery();

				if (!rs.next()) {
					return null;
				}

				
				String loginid2 = rs.getString("login_id");
				return loginid2;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {

				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
		
		
		public UserDataBeans information(String id) {
			Connection conn = null;

			try {

				conn = DBManager.getConnection();

				String sql = "SELECT * FROM user where id = ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, id);

				ResultSet rs = pStmt.executeQuery();


				if (!rs.next()) {
					return null;
				}

				int Id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String password = rs.getString("password");


				return new UserDataBeans(Id, loginId, name, password);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {

				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
		
		
		public void update(String loginid, String password, String name) {
			Connection conn = null;

			try {

				conn = DBManager.getConnection();

				String sql = "UPDATE user SET  password = ?, name = ? WHERE login_id=?";

				
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, password);
				pStmt.setString(2, name);
				pStmt.setString(3, loginid);

				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}
		}

		
		public void leave(String Id) {
			Connection conn = null;

			try {

				conn = DBManager.getConnection();

				String sql = "DELETE user, user_team FROM user"
						+ " LEFT OUTER JOIN user_team"
						+ " ON user.id = user_team.user_id"
						+ " WHERE user.id = ?;";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, Id);

				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();

			} finally {

				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}
		}
		
		
		
		public List<UserDataBeans> search(String loginId, String name) {
			Connection conn = null;
			List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

			try {

				conn = DBManager.getConnection();

				String sql = "SELECT * FROM user WHERE login_id != '1'";
				if (!loginId.equals("")) {
					sql += " and login_id = '" + loginId + "'";
				}
				if (!name.equals("")) {
					sql += " and name = '" + name + "'";
				}

				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				while (rs.next()) {
					int id = rs.getInt("id");
					String loginId1 = rs.getString("login_id");
					String name1 = rs.getString("name");
					String password = rs.getString("password");


					UserDataBeans user = new UserDataBeans(id, loginId1, name1, password);

					userList.add(user);

				}

			} catch (

			SQLException e) {
				e.printStackTrace();

			} finally {

				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}
			return userList;
		}
		
	 
	 
	public String code(String Pass) {
		//ハッシュを生成したい元の文字列
		String source = Pass;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);

		return result;
	}


}
