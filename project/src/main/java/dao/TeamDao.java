package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.TeamDataBeans;

public class TeamDao {

	public List<TeamDataBeans> findAll() {
		Connection conn = null;
		List<TeamDataBeans> teamList = new ArrayList<TeamDataBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM team"
					+ " JOIN league"
					+ " ON team.league_id = league.id";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String leagueId = rs.getString("league_id");
				String logo = rs.getString("logo");
				String name = rs.getString("name");
				String league = rs.getString("league");
				String area = rs.getString("area");

				TeamDataBeans team = new TeamDataBeans(id, leagueId, logo, name, league, area);

				teamList.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return teamList;
	}

	public TeamDataBeans findByteamId(String id) {
		TeamDataBeans tdb = new TeamDataBeans();
		Connection conn = null;
		
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM team WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				tdb.setId(rs.getInt("id"));
				tdb.setName(rs.getString("name"));
				tdb.setTeamURL(rs.getString("team_URL"));
				tdb.setFanclubURL(rs.getString("fanclub_URL"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return tdb;
	}
}
