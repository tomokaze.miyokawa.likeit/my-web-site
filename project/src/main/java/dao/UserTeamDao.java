package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserTeamDataBeans;

public class UserTeamDao {

	public List<UserTeamDataBeans> findAll(String userId) throws SQLException {
		Connection con = null;
		List<UserTeamDataBeans> userteamList = new ArrayList<UserTeamDataBeans>();

		try {
			con = DBManager.getConnection();

			PreparedStatement pStmt = con.prepareStatement(

					"SELECT team.name team_name,\n"
					+ "team.id team_id,\n"
					+ "team.logo,\n"
					+ "league.id league_id,\n"
					+ "league.area,\n"
					+ "league.league,\n"
					+ "user.id user_id,\n"
					+ "user_team.id\n"
					+ "\n"
					+ "FROM user_team\n"
					+ "JOIN user \n"
					+ "ON user_team.user_id = user.id\n"
					+ "JOIN team \n"
					+ "ON user_team.team_id = team.id\n"
					+ "JOIN league \n"
					+ "ON team.league_id = league.id\n"
					+ "WHERE user_team.user_id = ?");

			pStmt.setString(1, userId);
			ResultSet rs = pStmt.executeQuery();

			

			while (rs.next()) {
				UserTeamDataBeans utbd = new UserTeamDataBeans();	
				utbd.setId(rs.getInt("id"));
				utbd.setTeamId(rs.getInt("user_id"));
				utbd.setTeamId(rs.getInt("team_id"));
				utbd.setLeagueid(rs.getInt("league_id"));
				utbd.setName(rs.getString("team_name"));
				utbd.setLogo(rs.getString("logo"));
				utbd.setLeague(rs.getString("league"));
				utbd.setArea(rs.getString("area"));

				userteamList.add(utbd);
			}

			System.out.println("searching BuyDataBeans by buyID has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return userteamList;
	}

	public void updateTeam(int userId, String[] checkedTeams) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String deleteSql = "DELETE FROM user_team WHERE user_id = ?";
			PreparedStatement dpStmt = conn.prepareStatement(deleteSql);
			dpStmt.setInt(1, userId);
			dpStmt.executeUpdate();

			for (String teamId : checkedTeams) {
				String sql = "INSERT INTO user_team (team_id, user_id) VALUE(?,?)";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, teamId);
				pStmt.setInt(2, userId);

				pStmt.executeUpdate();
			}

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}
}
