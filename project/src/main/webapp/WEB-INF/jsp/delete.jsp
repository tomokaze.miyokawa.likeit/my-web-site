<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>投稿削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/delete.css" rel="stylesheet">
</head>

<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand" href="UserData?id=${userInfo.id}">${userInfo.name}</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><c:if test="${userInfo.loginId == '1'}">
							<a class="nav-link active" aria-current="page"
								href="UserList?id=${userInfo.id}">ユーザー一覧</a>
						</c:if></li>
					<li class="nav-item"><a class="nav-link" href="Category">カテゴリー一覧</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>

<body>
	<h1>投稿削除確認</h1>


	<h2>
		<table border="1" align="center">
			<div class="card">
				<div class="card-body">
					<div class="container">
						<div class="row">
							<div class="col">
								<h5 class="card-title">
									<a>${post.title}</a>
								</h5>
							</div>
							<div class="col-6"></div>
							<div class="col">
								<a href="UserData?id=${post.userId}">${post.name}</a> <a>${post.create_date}</a>
							</div>
						</div>
						<p class="card-text">${post.title_body}</p>

					</div>
				</div>
			</div>
		</table>

		<br> この投稿を削除してよろしいでしょうか。
	</h2>


	<div class="container">
		<div class="row row-cols-4">
			<div class="col"></div>
			<div class="col">
				<a href="Category" class="btn btn-secondary">キャンセル</a>
			</div>
			<div class="col">
				<form action="Delete" method="post">
					<input type="hidden" name="id" value="${post.id}">
					<button type="submit" class="btn btn-secondary">OK</button>
				</form>
			</div>
			<div class="col"></div>
		</div>
	</div>

</body>

</html>