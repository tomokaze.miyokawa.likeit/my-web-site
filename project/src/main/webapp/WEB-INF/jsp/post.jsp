<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>投稿一覧</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">
<link href="css/post.css" rel="stylesheet">
</head>
<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand" href="UserData?id=${userInfo.id}">${userInfo.name}</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><c:if test="${userInfo.loginId == '1'}">
							<a class="nav-link active" aria-current="page"
								href="UserList?id=${userInfo.id}">ユーザー一覧</a>
						</c:if></li>
					<li class="nav-item"><a class="nav-link" href="Category">カテゴリー一覧</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>

<body>



	<h1>投稿一覧</h1>

	<h2>
		<a href="${team.teamURL}" target="_blank">${team.name}</a>
	</h2>
	<h2>
		<a href="${team.fanclubURL}" target="_blank">ファンクラブ</a>
	</h2>
	<hr>
	<form action="Post" method="post">
		<table border="1" align="center">

			<c:if test="${errMsg != null}">
				<font color="#F00">${errMsg}</font>
			</c:if>

			<p>${userInfo.name}</p>
			<div class="mb-3 row">
				<label for="inputPassword" class="col-sm-2 col-form-label">タイトル</label>
				<div class="col-sm-10">
					<input type="text" name="title" class="form-control"
						id="inputPassword">
				</div>
			</div>
			<div class="mb-3 row">
				<label for="inputPassword" class="col-sm-2 col-form-label">内容</label>
				<div class="col-sm-10">
					<input type="hidden" name="id" value="${team.id}">
					<textarea class="form-control" id="exampleFormControlTextarea1"
						name="title_body" rows="3"></textarea>
				</div>
			</div>

			<div class="contribute">
				<button type="submit" class="btn btn-secondary mb-3">投稿</button>
			</div>
			</div>
		</table>
	</form>
	<hr>
	<c:forEach var="post" items="${postList}">
		<table border="1" align="center">
			<div class="card">
				<div class="card-body">
					<div class="container">
						<div class="row">
							<div class="col">
								<h5 class="card-title">
									<a>${post.title}</a>
								</h5>
							</div>
							<div class="col-6"></div>
							<div class="col">
								<a href="UserData?id=${post.userId}">${post.name}</a> <a>${post.create_date}</a>
							</div>
						</div>
						<p class="card-text">${post.title_body}</p>
						<div class="delete">
							<c:if
								test="${userInfo.loginId == '1' || userInfo.id == post.userId}">
								<a href="Delete?id=${post.id}" class="btn btn-danger">削除</a>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</table>
	</c:forEach>
</body>

</html>