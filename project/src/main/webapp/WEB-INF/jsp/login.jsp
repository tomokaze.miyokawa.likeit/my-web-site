<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/login.css" rel="stylesheet">
</head>

<body>
	<form action="Login" method="post">

		<c:if test="${errMsg != null}">
			<font color="#F00">${errMsg}</font>
		</c:if>

		<c:if test="${logoutMsg != null}">
			<font color="#008000">${logoutMsg}</font>
		</c:if>

		<c:if test="${leaveMsg != null}">
			<font color="#00F">${leaveMsg}</font>
		</c:if>


		<h1>ログイン画面</h1>
		<h2>
			ログインID<input type="text" name="loginId">
		</h2>

		<h2>
			パスワード<input type="password" name="password" value="">
		</h2>

		<div class="button-area">
			<button type="submit" class="btn btn-secondary">ログイン</button>

			<p><a href="Registration">新規登録</a></p>

		</div>
	</form>

	</div>
</body>

</html>