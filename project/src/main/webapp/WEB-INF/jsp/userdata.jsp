<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザー詳細</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">
<link href="css/userdata.css" rel="stylesheet">
</head>
<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand" href="UserData?id=${userInfo.id}">${userInfo.name}</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><c:if test="${userInfo.loginId == '1'}">
							<a class="nav-link active" aria-current="page"
								href="UserList?id=${userInfo.id}">ユーザー一覧</a>
						</c:if></li>
					<li class="nav-item"><a class="nav-link" href="Category">カテゴリー一覧</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>

<body>
	<form action="UserData" method="post">
		<h1>ユーザー詳細</h1>

		<c:if test="${errMsg != null}">
			<font color="#F00">${errMsg}</font>
		</c:if>

		<h2>
			ログインID ${user.loginId} <input type="hidden" name="loginId"
				value="${user.loginId}">
		</h2>
		<h2>
			<c:if test="${userInfo.loginId == '1' || userInfo.loginId == user.loginId}">
			パスワード<input type="text" name="password" value="">
			</c:if>
		</h2>
		<h2>
			<c:if
				test="${userInfo.loginId == '1' || userInfo.loginId == user.loginId}">
			パスワード(確認)<input type="text" name="password2">
			</c:if>
		</h2>
		<h2>
			ユーザー名<input type="text" name="name" value="${user.name}">
		</h2>

		<div class="container">
			<div class="row">
				<div class="col-10"></div>
				<c:if
					test="${userInfo.loginId == '1' || userInfo.loginId == user.loginId}">
					<div class="col-1">
						<button type="submit" class="btn btn-secondary">更新</button>
					</div>
			</div>
		</div>
		</c:if>
	</form>
	<div class="container">
		<div class="row">
			<div class="col">お気に入りのチーム</div>
			<div class="col"></div>
			<div class="col"></div>
			<div class="col">
				<c:if
					test="${userInfo.loginId == '1' || userInfo.loginId == user.loginId}">
					<a href="TeamList" class="btn btn-secondary">追加</a>
				</c:if>
			</div>
		</div>
	</div>

	<table class="table table-bordered" align="center">

		<tr>
			<th>リーグ・地区</th>
			<th>ロゴ</th>
			<th>チーム名</th>
		</tr>
		<c:forEach var="userteam" items="${userteamList}">
			<tr>
				<td>${userteam.league}${userteam.area}</td>
				<td><img src="img/${userteam.logo}"></td>
				<td>${userteam.name}</td>
			</tr>
		</c:forEach>
	</table>
	<c:if
		test="${userInfo.loginId == '1' || userInfo.loginId == user.loginId}">
		<a href="Leave?id=${user.id}" class="btn btn-danger">退会</a>
	</c:if>


</body>

</html>