<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>チーム一覧</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">
<link href="css/teamlist.css" rel="stylesheet">
</head>
<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand" href="UserData?id=${userInfo.id}">${userInfo.name}</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><c:if test="${userInfo.loginId == '1'}">
							<a class="nav-link active" aria-current="page"
								href="UserList?id=${userInfo.id}">ユーザー一覧</a>
						</c:if></li>
					<li class="nav-item"><a class="nav-link" href="Category">カテゴリー一覧</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>

<body>
	<form action="TeamList" method="post">
		<h1>チーム一覧</h1>

		<table class="table table-bordered" align="center">
			<tr>
				<th></th>
				<th>リーグ・地区</th>
				<th>ロゴ</th>
				<th>チーム名</th>
			</tr>
			<c:forEach var="team" items="${teamList}">
				<tr>
					<td><input class="form-check-input me-1" type="checkbox"
						value="${team.id}" aria-label="..." name="checkbox"></td>
					<td>${team.league}${team.area}</td>
					<td><img src="img/${team.logo}"></td>
					<td>${team.name}</td>
				</tr>
			</c:forEach>
		</table>
		<div class="button-area"></div>
		<button type="submit" class="btn btn-secondary">登録</button>
	</form>
</body>

</html>