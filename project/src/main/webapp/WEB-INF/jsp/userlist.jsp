<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">
<link href="css/userlist.css" rel="stylesheet">
</head>
<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand" href="UserData?id=${userInfo.id}">${userInfo.name}</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><c:if test="${userInfo.loginId == '1'}">
							<a class="nav-link active" aria-current="page"
								href="UserList?id=${userInfo.id}">ユーザー一覧</a>
						</c:if></li>
					<li class="nav-item"><a class="nav-link" href="Category">カテゴリー一覧</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>


	<body>

	<h1>ユーザ一覧</h1>

	<form action="UserList" method="post">
		<h2>
			ログインID<input type="text" name="loginId">
		</h2>
		<h2>
			ユーザー名<input type="text" name="name">
		</h2>

		<h3>
					<button type="submit" class="btn btn-primary">検索</button>
					</h3>
				<h3>
					<a href="Registration">新規登録</a>
					</h3>
	</form>
	<hr>
	<table class="table">
  <thead>
    <tr>
      <th scope="col">ログインID</th>
      <th scope="col">ユーザー名</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <c:forEach var="user" items="${userList}">
    <tr>
      <th scope="row">${user.loginId}</th>
      <td>${user.name}</td>
      <td><a href="UserData?id=${user.id}"
        >編集</a></td>
    </tr>
  </c:forEach>
  </tbody>
	</table>
	</body>

</html>