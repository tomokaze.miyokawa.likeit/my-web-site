<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>退会</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">
<link href="css/leave.css" rel="stylesheet">
</head>

<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand" href="UserData?id=${userInfo.id}">${userInfo.name}</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><c:if test="${userInfo.loginId == '1'}">
							<a class="nav-link active" aria-current="page"
								href="UserList?id=${userInfo.id}">ユーザー一覧</a>
						</c:if></li>
					<li class="nav-item"><a class="nav-link" href="Category">カテゴリー一覧</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>

<body>

	<h1>退会確認</h1>
	
	<a>${user.name}</a>

	<br> このユーザーを退会してよろしいでしょうか。


	<div class="container">
		<div class="row row-cols-4">
			<div class="col"></div>
			<div class="col">
				<a href="UserData" class="btn btn-secondary">キャンセル</a>
			</div>
			<div class="col">
				<form action="Leave" method="post">
				<input type="hidden" name="id" value="${user.id}">
					<button type="submit" class="btn btn-secondary">OK</button>
					</form>
			</div>
			<div class="col"></div>
		</div>
	</div>

</body>
</html>